# Requirements:
- JDK 8, Maven 3

# How to run:
```sh
$ mvn install
$ java -jar target/payments.jar
```

# Specification:

### Create an account:
```sh
POST http://localhost:8081/add-account/?number=1000&amount=900
```
Result:
 - **201** Created

### List accounts:
```sh
GET http://localhost:8081/accounts
```
Result:
 - **200** OK
 - JSON Body

### Retrieve amount by account number:
```sh
POST http://localhost:8081/account/1000/amount
```
Result:
 - **200** OK
 - JSON Body Number in format 0.00

### Make transfer:
```sh
POST http://localhost:8081/transfer/?src=1000&dest=1001&amount=900
```
Expected result:
 - **201** Created

### List all transfers:
```sh
GET http://localhost:8081/transfers
```
Expected result:
 - **200** OK
 - JSON Body

### Possible errors:
 - **400** Bad request / Not enough money