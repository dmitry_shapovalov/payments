package com.revolut.testtask.web;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.testtask.DependencyModule;
import com.revolut.testtask.service.AccountService;
import com.revolut.testtask.service.TransferService;
import io.vertx.core.Vertx;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class AppWebServerTest {

    public AppWebServerTest() {
        Vertx vertx = Vertx.vertx();
        Injector injector = Guice.createInjector(new DependencyModule());
        vertx.deployVerticle(injector.getInstance(AppWebServer.class));
        vertx.deployVerticle(injector.getInstance(AccountService.class));
        vertx.deployVerticle(injector.getInstance(TransferService.class));
    }

    @BeforeMethod
    public void before() {
        given().post("http://localhost:8081/add-account/?number=1000&amount=900");
        given().post("http://localhost:8081/add-account/?number=1001&amount=2100.500");
    }

    @Test
    public void shouldCreateAccounts() {
        when()
            .get("http://localhost:8081/accounts").
            then()
            .statusCode(HttpStatus.SC_OK)
            .body("[0].number", is(1000))
            .and()
            .body("[1].number", is(1001));

    }

    @Test
    public void shouldCreateTransfer() {
        given()
            .post("http://localhost:8081/transfer/?src=1000&dest=1001&amount=900");
        when()
            .get("http://localhost:8081/transfers")
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("[0].uuid", notNullValue());
    }

    @Test
    public void shouldPerformMoneyTransfer1() {
        given()
            .post("http://localhost:8081/transfer/?src=1000&dest=1001&amount=800");
        when()
            .get("http://localhost:8081/account/1000/amount")
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body(is("100.00"));
    }

    @Test
    public void shouldPerformMoneyTransfer2() {
        given()
            .post("http://localhost:8081/transfer/?src=1000&dest=1001&amount=800");
        when()
            .get("http://localhost:8081/account/1001/amount")
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body(is("2900.50"));
    }

    @Test
    public void shouldFailIfNotEnoughMoney() {
        when()
            .post("http://localhost:8081/transfer/?src=1000&dest=1001&amount=5000")
            .then()
            .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

}