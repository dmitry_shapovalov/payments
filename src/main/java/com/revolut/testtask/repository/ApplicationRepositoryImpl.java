package com.revolut.testtask.repository;

import com.revolut.testtask.model.Account;
import com.revolut.testtask.model.Transfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationRepositoryImpl implements ApplicationRepository {

    private final Map<Long, Account> accounts = new HashMap<>();
    private final Map<String, Transfer> transfers = new HashMap<>();

    @Override
    public List<Account> getAllAccounts() {
        return new ArrayList<>(accounts.values());
    }

    @Override
    public Account getAccountById(Long id) {
        return accounts.get(id);
    }

    @Override
    public List<Transfer> getAllTransfers() {
        return new ArrayList<>(transfers.values());
    }

    @Override
    public boolean addTransfer(Transfer transfer) {
        if (accounts.get(transfer.getSource()).getAmount().compareTo(transfer.getAmount()) >= 0) {
            Account sourceAcc = accounts.get(transfer.getSource());
            Account destinationAcc = accounts.get(transfer.getDestination());

            sourceAcc.setAmount(sourceAcc.getAmount().subtract(transfer.getAmount()));
            destinationAcc.setAmount(destinationAcc.getAmount().add(transfer.getAmount()));

            accounts.put(sourceAcc.getNumber(), sourceAcc);
            accounts.put(destinationAcc.getNumber(), destinationAcc);
            transfers.put(transfer.getUuid(), transfer);
            return true;
        }
        return false;
    }

    @Override
    public boolean addAccount(Account account) {
        accounts.put(account.getNumber(), account);
        return true;
    }
}