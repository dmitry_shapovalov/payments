package com.revolut.testtask.repository;

import com.revolut.testtask.model.Account;
import com.revolut.testtask.model.Transfer;

import java.util.List;

public interface ApplicationRepository {

    /**
     * Retrieves all accounts list
     * @return List of Accounts
     */
    List<Account> getAllAccounts();

    /**
     * Retrieves an Account by acc id.
     * @param id Account number
     * @return Account object
     */
    Account getAccountById(Long id);

    /**
     * Retrieves all transfers list
     * @return List of Transfers
     */
    List<Transfer> getAllTransfers();

    /**
     * Creates a new transfer
     * @param transfer Expects Transfer object prepared in advance
     * @return true - success,  false - transfer failed due to "not enough money" reasons
     */
    boolean addTransfer(Transfer transfer);

    /**
     * Creates a new account
     * @param account Expects Account object prepared in advance
     * @return true - success,  false - account failed to create
     */
    boolean addAccount(Account account);
}