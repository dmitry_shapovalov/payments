package com.revolut.testtask;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.testtask.service.AccountService;
import com.revolut.testtask.service.TransferService;
import com.revolut.testtask.web.AppWebServer;
import io.vertx.core.Vertx;

public class Application {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new DependencyModule());

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(injector.getInstance(AppWebServer.class));
        vertx.deployVerticle(injector.getInstance(AccountService.class));
        vertx.deployVerticle(injector.getInstance(TransferService.class));
    }

}
