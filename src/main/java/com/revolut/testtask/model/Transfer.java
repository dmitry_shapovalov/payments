package com.revolut.testtask.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode(of="uuid")
public class Transfer {

    private final String uuid = UUID.randomUUID().toString();
    private final Long source;
    private final Long destination;
    private final BigDecimal amount;

}
