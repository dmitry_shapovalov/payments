package com.revolut.testtask.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Account {

    private final Long number;
    private BigDecimal amount;

}
