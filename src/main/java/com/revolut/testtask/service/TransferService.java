package com.revolut.testtask.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.revolut.testtask.model.Transfer;
import com.revolut.testtask.repository.ApplicationRepository;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class TransferService extends AbstractVerticle {

    private final Gson gson;
    private final ApplicationRepository applicationRepository;

    @Inject
    public TransferService(Gson gson, ApplicationRepository applicationRepository) {
        this.gson = gson;
        this.applicationRepository = applicationRepository;
    }

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer("getAllTransfers", message -> {
            List<Transfer> transfers = applicationRepository.getAllTransfers();
            log.info("Retrieving all transfers... ");
            message.reply(gson.toJson(transfers));
        });
        vertx.eventBus().consumer("performTransfer", message -> {
            Transfer newTransfer = gson.fromJson(message.body().toString(), Transfer.class);
            boolean isSuccess = applicationRepository.addTransfer(newTransfer);
            if (isSuccess) {
                log.info(String.format("Transfer id [%s] added.", newTransfer.getUuid()));
                message.reply(null);
            } else {
                message.fail(400, "Transfer failed. Not enough balance.");
            }
        });
    }

}