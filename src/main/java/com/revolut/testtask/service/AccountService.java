package com.revolut.testtask.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.revolut.testtask.model.Account;
import com.revolut.testtask.repository.ApplicationRepository;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
public class AccountService extends AbstractVerticle {

    private final Gson gson;
    private final ApplicationRepository applicationRepository;

    @Inject
    public AccountService(Gson gson, ApplicationRepository applicationRepository) {
        this.gson = gson;
        this.applicationRepository = applicationRepository;
    }

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer("getAllAccounts", message -> {
            List<Account> accounts = applicationRepository.getAllAccounts();
            log.info("Retrieving all accounts.");
            message.reply(gson.toJson(accounts));
        });
        vertx.eventBus().consumer("getAmountByAccountId", message -> {
            Account account = applicationRepository.getAccountById((Long)(message.body()));
            log.info("Retrieving amount.");
            message.reply(gson.toJson(account.getAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN)));
        });
        vertx.eventBus().consumer("addAccount", message -> {
            Account newAccount = gson.fromJson(message.body().toString(), Account.class);
            boolean isSuccess = applicationRepository.addAccount(newAccount);
            if (isSuccess) {
                log.info(String.format("Account number [%s] added.", newAccount.getNumber()));
                message.reply(null);
            } else {
                message.fail(400, "Account failed to add.");
            }
        });
    }

}