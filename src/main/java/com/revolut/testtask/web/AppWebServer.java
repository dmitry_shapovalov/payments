package com.revolut.testtask.web;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.revolut.testtask.model.Account;
import com.revolut.testtask.model.Transfer;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_JSON;
import static io.netty.handler.codec.http.HttpResponseStatus.*;

@Slf4j
public class AppWebServer extends AbstractVerticle {

    @Inject
    private Gson gson;

    private HttpServer appWebServer = null;
    private static final int PORT = 8081;

    @Override
    public void start() throws Exception {
        log.info("Application has started at " + PORT);
        Router router = Router.router(vertx);
        router.get("/accounts").handler(this::getAccounts);
        router.get("/account/:number/amount").handler(this::getAmountByAccountId);
        router.get("/transfers").handler(this::getTransfers);
        router.post("/transfer").handler(this::addTransfer);
        router.post("/add-account").handler(this::addAccount);

        vertx.createHttpServer()
            .requestHandler(router::accept)
            .listen(PORT);
    }

    private void getAccounts(RoutingContext routingContext) {
        log.info("Received all accounts request.");
        vertx.eventBus().send("getAllAccounts", null, reply -> processGetReply(routingContext, reply));
    }

    private void getAmountByAccountId(RoutingContext routingContext) {
        log.info("Received account request.");
        Long number = Long.valueOf(routingContext.request().getParam("number"));
        vertx.eventBus().send("getAmountByAccountId", number, reply -> processGetReply(routingContext, reply));
    }

    private void getTransfers(RoutingContext routingContext) {
        log.info("Received all transfers request.");
        vertx.eventBus().send("getAllTransfers", null, reply -> processGetReply(routingContext, reply));
    }

    private void addAccount(RoutingContext routingContext) {
        log.info("Received add account request.");
        MultiMap queryParams = routingContext.queryParams();
        List<String> paramsNames = Arrays.asList("number", "amount");
        if (!Collections.disjoint(queryParams.names(), paramsNames)) {
            Account account = new Account(
                Long.valueOf(queryParams.get("number")),
                new BigDecimal(queryParams.get("amount")).setScale(2, BigDecimal.ROUND_HALF_EVEN));

            vertx.eventBus().send("addAccount", gson.toJson(account, Account.class), reply -> {
                if (reply.succeeded()) {
                    log.info("Reply: Account added.");
                    sendHttpResponse(routingContext, CREATED.code());
                } else {
                    log.info("Reply: Account creation failed." + reply.cause());
                    sendHttpResponse(routingContext, BAD_REQUEST.code());
                }
            });
        }
    }

    private void addTransfer(RoutingContext routingContext) {
        log.info("Received transfer request.");
        MultiMap queryParams = routingContext.queryParams();
        List<String> paramsNames = Arrays.asList("src", "dest", "amount");
        if (!Collections.disjoint(queryParams.names(), paramsNames)) {
            Transfer transfer = new Transfer(
                Long.valueOf(queryParams.get("src")),
                Long.valueOf(queryParams.get("dest")),
                new BigDecimal(queryParams.get("amount")).setScale(2, BigDecimal.ROUND_HALF_EVEN));

            vertx.eventBus().send("performTransfer", gson.toJson(transfer, Transfer.class), reply -> {
                if (reply.succeeded()) {
                    log.info("Reply: Transferred.");
                    sendHttpResponse(routingContext, CREATED.code());
                } else {
                    log.info("Reply: Transfer failed." + reply.cause());
                    sendHttpResponse(routingContext, BAD_REQUEST.code());
                }
            });
        }
    }

    @Override
    public void stop() throws Exception {
        appWebServer.close();
        super.stop();
    }

    private void processGetReply(RoutingContext routingContext, AsyncResult<Message<Object>> reply) {
        if (reply.succeeded()) {
            log.info("Reply: " + reply.result().body());
            sendHttpResponse(routingContext, OK.code(), reply.result().body().toString());
        } else {
            log.info("Reply: No reply from cluster received.");
            sendHttpResponse(routingContext, INTERNAL_SERVER_ERROR.code());
        }
    }

    private void sendHttpResponse(RoutingContext context, int status) {
        context.response()
            .putHeader("content-type", APPLICATION_JSON)
            .setStatusCode(status)
            .end();
    }

    private void sendHttpResponse(RoutingContext context, int status, String messageBody) {
        context.response()
            .putHeader("content-type", APPLICATION_JSON)
            .setStatusCode(status)
            .end(messageBody);
    }

}