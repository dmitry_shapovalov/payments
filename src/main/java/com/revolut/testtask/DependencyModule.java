package com.revolut.testtask;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.revolut.testtask.repository.ApplicationRepository;
import com.revolut.testtask.repository.ApplicationRepositoryImpl;

public class DependencyModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ApplicationRepository.class).to(ApplicationRepositoryImpl.class).in(Scopes.SINGLETON);
    }

}
